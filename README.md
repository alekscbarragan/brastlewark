# Brastlewark

### Libraries
Brastlewark project does not use any party third party libraries. The reason behind this decision was because the instructions establish that I needed to show I know the basics. 

## What was missing & wished to be implemented.
- Configurations
    - I wanted to implement an option where users can change how gnomes are displayed. `GridType` was not used to be set by user.
    - I wanted to implement a light and dark mode. `UIMode` implementation is missing.

- Image cache
    - Only a simple memory cache was implement. I did not implement disk cache.

- Reachability
    - Currently, the app does not check if it has internet connectivity. 

- Better UI.