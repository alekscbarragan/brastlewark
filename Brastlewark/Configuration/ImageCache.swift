//
//  ImageCache.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

class ImageCache {
    private init() { }
    static let `default` = ImageCache()
    private let memoryCache = NSCache<NSString, UIImage>()

    func store(_ image: UIImage, forKey key: String) {
        memoryCache.setObject(image, forKey: key as NSString)
    }

    func retrieveImage(forKey key: String) -> UIImage? {
        return memoryCache.object(forKey: key as NSString)
    }
}
