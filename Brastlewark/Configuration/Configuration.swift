//
//  Configuration.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/6/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation
import CoreGraphics

protocol ConfigurationType {
    var title: String { get }
    var detail: String { get }
    var currentValue: String { get }
}

/// Grid layout for gnomes.
enum GridType: ConfigurationType {
    case compact
    case extended
    case column
    
    var title: String {
        return "Grid"
    }
    
    var detail: String {
        return "How gnomes are displayed."
    }
    
    var currentValue: String {
        switch self {
        case .compact:
            return "Compact"
            
        case .column:
            return "Columns"
            
        case .extended:
            return "Extended"
        }
    }
    
    func size(maxWidth: CGFloat) -> CGSize {
        let padding: CGFloat = 30.0
        var size: CGSize
        switch self {
        case .column:
            let totalWidth = (maxWidth - CGFloat(padding)) / 2
            size = CGSize(width: totalWidth, height: 200)
            
        case .compact:
            let totalWidth = (maxWidth - CGFloat(padding)) // Divide by two for two columns. / 2
            size = CGSize(width: totalWidth, height: 50)
            
        case .extended:
            let totalWidth = (maxWidth - CGFloat(padding)) // Divide by two for two columns. / 2
            size = CGSize(width: totalWidth, height: 100)
        }
        
        return size
    }
}

/// Theme for the app. Users may be able to select `light` or `dark` theme.
enum UIMode: ConfigurationType {
    case light
    case dark
    
    var title: String {
        return "UI Mode"
    }
    
    var detail: String {
        return "Show a light or dark UI"
    }
    
    var currentValue: String {
        switch self {
        case .light:
            return "Light"
        
        case .dark:
            return "Dark"
        }
    }
    
}
