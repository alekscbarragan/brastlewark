//
//  Repository.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/10/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

protocol Repository {
    associatedtype T
    var items: [T] { get set }
    func find(id: Int) -> T?
    func find(by name: String) -> T?
    func itemAt(index: Int) -> T?
    func update(items: [T])
    func fetch(completion: ((Result<[T]>) -> Void)?)
    var result: ((Result<[T]>) -> Void)? { get set }
}
