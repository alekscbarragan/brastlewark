//
//  OperationResult.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/8/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

protocol OperationResult {
    associatedtype T
    var result: ((Result<T>) -> Void)? { get set }
}
