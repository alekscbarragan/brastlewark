//
//  Reusable.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/6/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

protocol Reusable {
    static var reuseIdentifier: String { get }
}

extension Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
