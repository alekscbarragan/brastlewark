//
//  Coordinator.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

final class Coordinator: BaseCoordinator {
    private var currentViewController: BaseViewController {
        guard let vc = navigationController.topViewController as? BaseViewController else {
            preconditionFailure("`BaseViewController` expected.")
        }
        
        return vc
    }
    
    private var gnomeRepository = GnomeRepository()
    private let gridType = GridType.extended
    private let uiMode = UIMode.light
    
    let navigationController: UINavigationController

    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let gnomesViewController = GnomesViewController()
        gnomesViewController.delegate = self
        navigationController.pushViewController(gnomesViewController, animated: false)
    }

}

// MARK: - Gnomes View Controller Delegate
extension Coordinator: GnomesViewControllerDelegate {
    func gnomesViewController(_ viewController: GnomesViewController, didSelect gnome: Gnome) {
        let gnomeDetailViewController = GnomeDetailViewController(gnome: gnome, repository: gnomeRepository)
        gnomeDetailViewController.delegate = self
        navigationController.pushViewController(gnomeDetailViewController, animated: true)
    }
    
    func gnomesViewController(_ viewController: GnomesViewController, didUpdateGnome respository: GnomeRepository) {
        gnomeRepository = respository
    }

}

// MARK: - Gnomes Detail View Controller Delegate
extension Coordinator: GnomeDetailViewControllerDelegate {
    func gnomeDetailViewController(_ viewController: GnomeDetailViewController, didSelectFriend gnome: Gnome) {
        let gnomeDetailViewController = GnomeDetailViewController(gnome: gnome, repository: gnomeRepository)
        gnomeDetailViewController.delegate = self
        navigationController.pushViewController(gnomeDetailViewController, animated: true)
    }

}
