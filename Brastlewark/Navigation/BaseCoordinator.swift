//
//  BaseCoordinator.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

protocol BaseCoordinator: class {
    var navigationController: UINavigationController { get }
    func start()
}
