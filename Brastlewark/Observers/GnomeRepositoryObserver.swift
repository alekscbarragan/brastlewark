//
//  GnomeRepositoryObserver.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

struct GnomeRepositoryObserver: OperationObserver, OperationResult {
    var result: ((Result<[Gnome]>) -> Void)?
    func operationDidFinish(operation: KavakOperation) {

        guard let operation = operation as? DataSourceOperation, let gnomes = operation.generatedModel?.gnomes else {
            return
        }

        let sortedGnomes = gnomes.sorted { (arg0, arg1) -> Bool in
            return arg0.name < arg1.name
        }

        result?(.success(sortedGnomes))
    }

    func operation(operation: KavakOperation, didFinishWithErrors errors: [Error]) {

        guard let error = errors.first else {
            result?(.failure(ImageOperationError.unknown))
            return
        }

        result?(.failure(error))
    }
}
