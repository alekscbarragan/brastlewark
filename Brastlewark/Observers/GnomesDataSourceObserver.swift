//
//  GnomesDataSourceObserver.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

struct GnomesDataSourceObserver: OperationObserver {
    
    private weak var viewController: GnomesViewController?
    
    var result: ((Result<[Gnome]>) -> Void)?
    
    init(viewController: GnomesViewController) {
        self.viewController = viewController
        viewController.startIndicatorViewAnimation()
    }
    
    func operationDidFinish(operation: KavakOperation) {
        guard let operation = operation as? DataSourceOperation, let gnomes = operation.generatedModel?.gnomes else {
            return
        }
        
        let sortedGnomes = gnomes.sorted { (arg0, arg1) -> Bool in
            return arg0.name < arg1.name
        }
        
        result?(.success(sortedGnomes))
        
        viewController?.updateDataSource(with: sortedGnomes)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.viewController?.stopIndicatorViewAnimation()
        }
        
    }
    
    func operation(operation: KavakOperation, didFinishWithErrors errors: [Error]) {
        guard let error = errors.first else {
            return
        }
        
        print(error.localizedDescription)
        
    }
}
