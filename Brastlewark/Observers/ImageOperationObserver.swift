//
//  ImageOperationObserver.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

struct ImageOperationObserver: OperationObserver, OperationResult {

    var result: ((Result<UIImage>) -> Void)?

    func operationDidFinish(operation: KavakOperation) {
        guard let operation = operation as? NetworkImageOperation, let image = operation.downloadedImage else {
            return
        }

        result?(.success(image))
    }

    func operation(operation: KavakOperation, didFinishWithErrors errors: [Error]) {
        guard let error = errors.first else {
            return
        }

        result?(.failure(error))
    }
}
