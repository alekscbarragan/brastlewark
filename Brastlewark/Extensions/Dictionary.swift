//
//  Dictionary.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

extension Dictionary where Key: ExpressibleByStringLiteral, Value: ExpressibleByStringLiteral {
    public func toURLEncodedString() -> String {
        var pairs = [String]()
        for element in self {
            if let key = KavakAPI.encode(element.0 as AnyObject),
                let value = KavakAPI.encode(element.1 as AnyObject), (!value.isEmpty && !key.isEmpty) {
                pairs.append([key, value].joined(separator: "="))
            } else {
                continue
            }
        }
        
        guard !pairs.isEmpty else {
            return ""
        }
        
        return pairs.joined(separator: "&")
    }
}
