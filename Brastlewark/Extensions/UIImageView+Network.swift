//
//  UIImageView+.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImage(withUrlString urlString: String , queue: OperationQueue) {
        let operation = NetworkImageOperation(urlString: urlString)
        var observer = ImageOperationObserver()
        observer.result = { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let image):
                OperationQueue.main.addOperation {
                    self.image = image
                }

            case .failure(let error):
                print(error)
            }
        }

        operation.add(observer: observer)
        queue.addOperation(operation)
    }
}
