//
//  UIView+Autolayout.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/6/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: - Edges
    func pinToEdges(insets: UIEdgeInsets = .zero, safeArea: Bool = false, activate: Bool = false) -> [NSLayoutConstraint] {
        guard superview != nil else { fatalError("Superview is needed for autolayout. No superview found.") }
        translatesAutoresizingMaskIntoConstraints = false
        let constraints = [leftToSuperView(offset: insets.left, activate: activate),
                           rightToSuperView(offset: insets.right, activate: activate),
                           topToSuperview(offset: insets.top, safeArea: safeArea, activate: activate),
                           bottomToSuperview(offset: insets.bottom, activate: activate)]
        
        return constraints
    }
    
    // MARK: - Right
    @discardableResult
    func right(to view: UIView,
               anchor: NSLayoutXAxisAnchor? = nil,
               offset: CGFloat = 0,
               activate: Bool = false) -> NSLayoutConstraint {
        return makeConstraint(from: rightAnchor, equalTo: anchor ?? view.rightAnchor, offset: offset, activate: activate)
    }
    
    @discardableResult
    func rightToLeft(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: rightAnchor, equalTo: view.leftAnchor, offset: -offset, activate: activate)
    }
    
    @discardableResult
    func rightToSuperView(offset: CGFloat = 0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }

        return makeConstraint(from: rightAnchor, equalTo: superview.rightAnchor, offset: offset, activate: activate)
        
    }
    
    // MARK: - Left
    @discardableResult
    func left(to view: UIView,
              anchor: NSLayoutXAxisAnchor? = nil,
              offset: CGFloat = 0,
              activate: Bool = false) -> NSLayoutConstraint {
        let _anchor = anchor ?? view.leftAnchor
        return makeConstraint(from: leftAnchor, equalTo: _anchor, offset: offset, activate: activate)
    }
    
    @discardableResult
    func leftToRight(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: leftAnchor, equalTo: view.rightAnchor, offset: offset, activate: activate)
    }
    
    @discardableResult
    func leftToSuperView(offset: CGFloat = 0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        return makeConstraint(from: leftAnchor, equalTo: superview.leftAnchor, offset: offset, activate: activate)
    }
    
    // MARK: - Top
    @discardableResult
    func top(to view: UIView,
             anchor: NSLayoutYAxisAnchor? = nil,
             offset: CGFloat = 0,
             safeArea: Bool = false,
             activate: Bool = false) -> NSLayoutConstraint {
        
        let neighborAnchor: NSLayoutYAxisAnchor
        if safeArea {
            neighborAnchor = anchor ?? view.safeAreaLayoutGuide.topAnchor
        } else {
            neighborAnchor = anchor ?? view.topAnchor
        }
        
        return makeConstraint(from: topAnchor, equalTo: neighborAnchor, offset: offset, activate: activate)
    }
    
    @discardableResult
    func topToBottom(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: topAnchor, equalTo: view.bottomAnchor, offset: offset, activate: activate)
    }
    
    @discardableResult
    func topToSuperview(offset: CGFloat = 0, safeArea: Bool = false, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        
        let neighborAnchor: NSLayoutYAxisAnchor = safeArea ? superview.safeAreaLayoutGuide.topAnchor : superview.topAnchor
        
        return makeConstraint(from: topAnchor, equalTo: neighborAnchor, offset: offset, activate: activate)
    }
    
    // MARK: - Bottom
    @discardableResult
    func bottom(to view: UIView,
                anchor: NSLayoutYAxisAnchor? = nil,
                offset: CGFloat = 0,
                activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: bottomAnchor, equalTo: anchor ?? view.bottomAnchor, offset: offset, activate: activate)
    }
    
    @discardableResult
    func bottomToTop(of view: UIView,
                     offset: CGFloat = 0,
                     activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: bottomAnchor, equalTo: view.topAnchor, offset: offset, activate: activate)
    }
    
    @discardableResult
    func bottomToSuperview(offset: CGFloat = 0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }
        return makeConstraint(from: bottomAnchor, equalTo: superview.bottomAnchor, offset: offset, activate: activate)
        
    }
    
    // MARK: - Width
    @discardableResult
    func width(to view: UIView, multiplier: CGFloat = 1.0, activate: Bool = false) -> NSLayoutConstraint {
        let constraint = widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier)
        constraint.isActive = activate
        return constraint
    }

    @discardableResult
    func heightToSuperView(multiplier: CGFloat = 1.0, activate: Bool = false) -> NSLayoutConstraint {
        guard let superview = superview else {
            fatalError("Superview is needed for autolayout. No superview found.")
        }

        let constraint = heightAnchor.constraint(equalTo: superview.heightAnchor, multiplier: multiplier)
        constraint.isActive = activate
        return constraint
    }

    @discardableResult
    func height(equalToConstant: CGFloat = 0, multiplier: CGFloat = 1.0, activate: Bool = false) -> NSLayoutConstraint {
        let constraint = heightAnchor.constraint(equalToConstant: equalToConstant)
        constraint.isActive = activate
        return constraint
    }
    
    // MARK: - Center
    @discardableResult
    func center(of view: UIView) -> (xConstraint: NSLayoutConstraint, yConstraint: NSLayoutConstraint) {
        let xConstraint = centerX(of: view)
        let yConstraint = centerY(of: view)
        return (xConstraint, yConstraint)
    }
    
    @discardableResult
    func centerX(of view: UIView, offset: CGFloat = 0, activate: Bool = false) -> NSLayoutConstraint {
        return makeConstraint(from: centerXAnchor, equalTo: view.centerXAnchor, offset: offset, activate: activate)
        
    }
    
    @discardableResult
    func centerY(of view: UIView, offset: CGFloat = 0, activate: Bool = true) -> NSLayoutConstraint {
        return makeConstraint(from: centerYAnchor, equalTo: view.centerYAnchor, offset: offset, activate: activate)
    }
    
    // MARK: - Private
    private func makeConstraint(from anchor: NSLayoutXAxisAnchor,
                                equalTo neighborAnchor: NSLayoutXAxisAnchor,
                                offset: CGFloat,
                                activate: Bool) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: neighborAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
    private func makeConstraint(from anchor: NSLayoutYAxisAnchor,
                                equalTo neighborAnchor: NSLayoutYAxisAnchor,
                                offset: CGFloat,
                                activate: Bool) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: neighborAnchor, constant: offset)
        constraint.isActive = activate
        return constraint
    }
    
}

extension Array where Element == NSLayoutConstraint {
    func activate() {
        NSLayoutConstraint.activate(self)
    }
    func deactivate() {
        NSLayoutConstraint.deactivate(self)
    }
    func setActive(_ isActive: Bool) {
        if isActive {
            NSLayoutConstraint.activate(self)
        } else {
            NSLayoutConstraint.deactivate(self)
        }
    }
}
