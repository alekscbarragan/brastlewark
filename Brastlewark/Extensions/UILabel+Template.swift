//
//  UILabel+Template.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

extension UILabel {
    static func templateLabel(withText text: String,
                              autolayout: Bool = true,
                              adjustsFontSizeToFitWidth: Bool = true,
                              fontSize: CGFloat = 15,
                              weight: UIFont.Weight = .regular,
                              textAlignment: NSTextAlignment = .left) -> UILabel {
        let label = UILabel()
        label.textAlignment = textAlignment
        label.numberOfLines = 0
        label.text = text
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = !autolayout
        label.adjustsFontSizeToFitWidth = adjustsFontSizeToFitWidth
        label.minimumScaleFactor = 0.5
        label.font = UIFont.systemFont(ofSize: fontSize, weight: weight)
        return label
    }
}
