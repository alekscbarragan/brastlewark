//
//  UITableView+Reusable.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/6/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

extension UITableViewCell: Reusable { }

extension UITableView {
    func register(cellType type: UITableViewCell.Type) {
        register(type, forCellReuseIdentifier: type.reuseIdentifier)
    }
    
    func dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("You need to register cell of type `\(T.reuseIdentifier)`")
        }
        
        return cell
    }
}
