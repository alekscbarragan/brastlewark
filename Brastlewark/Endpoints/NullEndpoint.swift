//
//  NullEndpoint.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

struct NullEndpoint: EndpointConvertible {
    
    var baseURLString: String {
        return "https://httpbin.org"
    }
    
    func toURL() throws -> URL {
        throw EndpointError.invalidURL
    }
    
    func toURL(params: [String : String]) throws -> URL {
        throw EndpointError.invalidURL
    }
}
