//
//  EndpointConvertible.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

protocol EndpointConvertible {
    var baseURLString: String { get }
    func toURL() throws -> URL
    func toURL(params: [String: String]) throws -> URL
}

enum EndpointError: Error {
    case invalidURL
}
