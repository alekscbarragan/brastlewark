//
//  Brastlewark.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

enum BrastlewarkEndpoint: String, EndpointConvertible {
    case data = "data.json"
    
    var baseURLString: String {
        return "https://raw.githubusercontent.com/rrafols/mobile_test/master/"
    }
    
    func toURL() throws -> URL {
        guard let url = URL(string: "\(baseURLString)\(rawValue)") else {
            throw EndpointError.invalidURL
        }
        
        return url
    }
    
    func toURL(params: [String : String]) throws -> URL {
        let encodedParams = params.toURLEncodedString()
        guard let url = URL(string: "\(baseURLString)/\(rawValue)?\(encodedParams)") else {
            throw EndpointError.invalidURL
        }
        
        return url
    }
    
}
