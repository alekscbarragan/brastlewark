//
//  CollectionViewDataSource.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

final class CollectionViewDataSource<Cell: UICollectionViewCell, T>: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    typealias ConfigureCell = (Cell, T, IndexPath, UICollectionView) -> Void
    var items: [T]
    let cellIdentifier: String
    let configureCell: ConfigureCell
    var commonInset = UIEdgeInsets(top: 20, left: 5, bottom: 20, right: 10)
    
    private var didSelectItemAt: ((IndexPath, Cell, T, UICollectionView) -> Void)?
    private var willDisplayCell: ((Cell, IndexPath, T, UICollectionView) -> Void)?
    private var sizeForItemAt: ((IndexPath, UICollectionView, UICollectionViewLayout) -> CGSize)?
    
    init(withItems items: [T], cellIdentifier: String, configureCell: @escaping ConfigureCell) {
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.configureCell = configureCell
        super.init()
    }
    
    func updateItems(_ items: [T]) {
        self.items = items
    }
    
    // MARK: - Collection view data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(for: indexPath) as Cell
        let item = items[indexPath.row]
        configureCell(cell, item, indexPath, collectionView)
        return cell
    }
    
    // MARK: - Collection view delegate
    func didSelectAt(didSelectItemAt: @escaping ((IndexPath, Cell, T, UICollectionView) -> Void)) {
        self.didSelectItemAt = didSelectItemAt
    }
    
    func willDisplayCell(completion: @escaping ((Cell, IndexPath, T, UICollectionView) -> Void)) {
        willDisplayCell = completion
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? Cell else { return }
        willDisplayCell?(cell, indexPath, items[indexPath.row], collectionView) 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? Cell else { return }
        didSelectItemAt?(indexPath, cell, items[indexPath.row], collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {        
        return sizeForItemAt?(indexPath, collectionView, collectionViewLayout) ?? .zero
    }

    func sizeForItemAt(completion: @escaping (_ indexPath: IndexPath, _ collectionView: UICollectionView, _ viewLayout: UICollectionViewLayout) -> CGSize) {
        sizeForItemAt = completion
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return commonInset
    }
}
