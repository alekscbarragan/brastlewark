//
//  Result.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/8/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
