//
//  BaseViewController.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    lazy var indicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicatorView
    }()
    
    func startIndicatorViewAnimation() {
        guard indicatorView.superview == nil else { return }
        view.addSubview(indicatorView)
        indicatorView.pinToEdges().activate()
        indicatorView.startAnimating()
    }
    
    func stopIndicatorViewAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.indicatorView.alpha = 0.0
        }, completion: { finished in
            if finished {
                self.indicatorView.stopAnimating()
                self.indicatorView.alpha = 1.0
                self.indicatorView.removeFromSuperview()
            }
        })
    }
}
