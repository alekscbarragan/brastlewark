//
//  ArrangedScrollView.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/10/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

final class ArrangedScrollView: UIScrollView {
    private let stackView: UIStackView = {
        let stackView = UIStackView(axis: .vertical)
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    var insets: UIEdgeInsets = .zero {
        didSet {
            stackView.layoutMargins = insets
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(stackView)
        stackView.pinToEdges().activate()
        stackView.width(to: self, activate: true)
        alwaysBounceVertical = false
        stackView.isLayoutMarginsRelativeArrangement = true
        clipsToBounds = false
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Add all the given UIView instances as arranged subviews
    ///
    /// - Parameter subviews: All the UIView instances to be added as subviews
    func addArranged(subviews: UIView...) {
        subviews.forEach(stackView.addArrangedSubview)
    }

}

extension UIStackView {

    convenience init(axis: NSLayoutConstraint.Axis) {
        self.init()
        self.axis = axis
        translatesAutoresizingMaskIntoConstraints = false
    }

    /// Add all the given UIView instances as arranged subviews
    ///
    /// - Parameter subviews: All the UIView instances to be added as subviews
    func addArranged(subviews: UIView...) {
        addArranged(subviews: subviews)
    }

    /// Add all the given UIView instances as arranged subviews
    ///
    /// - Parameter subviews: Array containing the UIView instances to be added as subviews
    func addArranged(subviews: [UIView]) {
        subviews.forEach(addArrangedSubview)
    }

    /// Removes all the arranged subviews of the stackview,
    /// but first it deactivates all their constraints
    func removeAllArrangedSubviews() {
        arrangedSubviews.forEach({ $0.removeFromSuperview() })
    }

}
