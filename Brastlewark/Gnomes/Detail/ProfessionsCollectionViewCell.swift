//
//  ProfessionsCollectionViewCell.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

final class ProfessionsCollectionViewCell: UICollectionViewCell {
    private let nameLabel = UILabel.templateLabel(withText: "Name", textAlignment: .center)
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupConstraints() {
        contentView.addSubview(nameLabel)
        nameLabel.pinToEdges().activate()
    }

    func set(name: String) {
        nameLabel.text = name
    }

    func prepareForDisplay() {
        contentView.layoutIfNeeded()
        nameLabel.backgroundColor = .white
        nameLabel.layer.borderWidth = 1.0
        nameLabel.layer.borderColor = UIColor.black.cgColor
        nameLabel.layer.cornerRadius = nameLabel.frame.height / 2
        nameLabel.clipsToBounds = true
        nameLabel.layer.masksToBounds = true
    }
}
