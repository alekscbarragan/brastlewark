//
//  GnomeDetailViewController.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/8/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

protocol GnomeDetailViewControllerDelegate: class {
    func gnomeDetailViewController(_ viewController: GnomeDetailViewController, didSelectFriend gnome: Gnome)
}

final class GnomeDetailViewController: BaseViewController {

    // MARK: - Public Properties
    var repository = GnomeRepository()
    weak var delegate: GnomeDetailViewControllerDelegate?

    // MARK: - Private Properties
    typealias FriendsDataSourceType = CollectionViewDataSource<FriendCollectionViewCell, String>
    private lazy var friendsDataSource: FriendsDataSourceType = {
        let dataSource = FriendsDataSourceType(withItems: gnome.friends,
                                               cellIdentifier: FriendCollectionViewCell.reuseIdentifier,
                                               configureCell: { (cell, item, indexPath, collectionView) in
                                                cell.set(name: item)
        })
        dataSource.commonInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 10)
        return dataSource
    }()

    typealias ProfessionsDataSourceType = CollectionViewDataSource<ProfessionsCollectionViewCell, String>
    private lazy var professionsDataSource: ProfessionsDataSourceType = {
        let dataSource = ProfessionsDataSourceType(withItems: gnome.professions,
                                                   cellIdentifier: ProfessionsCollectionViewCell.reuseIdentifier,
                                                   configureCell: { (cell, item, indexPath, collectionView) in
                                                    cell.set(name: item)
        })
        dataSource.commonInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 10)
        return dataSource
    }()

    private let detailView = GnomeDetailView(thumbnailHeight: UIScreen.main.bounds.height * 0.33)
    private let gnome: Gnome
    private let queue = OperationQueue()
    
    init(gnome: Gnome) {
        self.gnome = gnome
        super.init(nibName: nil, bundle: nil)
    }

    convenience init(gnome: Gnome, repository: GnomeRepository) {
        self.init(gnome: gnome)
        self.repository = repository
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = detailView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(with: gnome)
        friendsDataSourceConfiguration()
        professionsDataSourceConfiguration()
    }

    func configure(with gnome: Gnome) {
        detailView.set(name: gnome.name, age: "\(gnome.age)", height: "\(gnome.height)", weight: "\(gnome.weight)")
        detailView.setThumbnail(withUrlString: gnome.thumbnail, queue: queue)
        detailView.isFriendsViewHidden = gnome.friends.isEmpty
        detailView.isProfessionsViewHidden = gnome.professions.isEmpty
    }

    private func friendsDataSourceConfiguration() {
        let inset = friendsDataSource.commonInset
        friendsDataSource.sizeForItemAt { (indexPath, collectionView, viewLayout) -> CGSize in
            let width = collectionView.frame.height - (inset.top + inset.bottom)
            let height = width
            return CGSize(width: width, height: height)
        }

        friendsDataSource.willDisplayCell { (cell, _, _, _) in
            cell.prepareForDisplay()
        }

        friendsDataSource.didSelectAt { [weak self] (indexPath, cell, item, collectionView) in
            guard let self = self,
                let gnome = self.repository.find(by: item),
                let delegate = self.delegate
                else {
                    return
            }

            delegate.gnomeDetailViewController(self, didSelectFriend: gnome)

        }

        detailView.configureFriendsCollectionView(dataSource: friendsDataSource)
        detailView.reloadFriends()
    }

    private func professionsDataSourceConfiguration() {
        let inset = professionsDataSource.commonInset
        professionsDataSource.sizeForItemAt { (indexPath, collectionView, viewLayout) -> CGSize in
            let width = collectionView.frame.height - (inset.top + inset.bottom)
            let height = width
            return CGSize(width: width, height: height)
        }

        professionsDataSource.willDisplayCell { (cell, _, _, _) in
            cell.prepareForDisplay()
        }

        detailView.configureProfessionsCollectionView(dataSource: professionsDataSource)
        detailView.reloadProfessions()

    }
}
