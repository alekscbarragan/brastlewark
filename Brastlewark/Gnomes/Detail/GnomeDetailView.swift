//
//  GnomeDetailView.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/10/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

final class GnomeDetailView: UIView {
    private let arrangedScrollView = ArrangedScrollView()
    private let nameLabel = UILabel.templateLabel(withText: "Name", fontSize: 20, weight: .bold, textAlignment: .center)
    private let ageLabel = UILabel.templateLabel(withText: "Age:\n10000", textAlignment: .center)
    private let heightLabel = UILabel.templateLabel(withText: "Height:\n9999", textAlignment: .center)
    private let weightLabel = UILabel.templateLabel(withText: "Weight:\n12345", textAlignment: .center)
    private let thumbnailImageView = UIImageView()

    private let friendsLabel = UILabel.templateLabel(withText: "Friends", fontSize: 20, weight: .bold, textAlignment: .center)
    private let friendsContainerView: UIView = {
        let view =  UIView()
        view.backgroundColor = .clear
        return view
    }()

    private let professionsLabel = UILabel.templateLabel(withText: "Professions", fontSize: 20, weight: .bold, textAlignment: .center)
    private let professionsContainerView: UIView = {
        let view =  UIView()
        view.backgroundColor = .clear
        return view
    }()

    private let friendsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()

    private let professionsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()

    private var thumbnailHeight: CGFloat {
        didSet {
            setupConstraints()
        }
    }

    var insets: UIEdgeInsets = .zero {
        didSet {
            arrangedScrollView.insets = insets
        }
    }

    var isFriendsViewHidden: Bool = false {
        didSet {
            friendsLabel.isHidden = isFriendsViewHidden
            friendsContainerView.isHidden = isFriendsViewHidden
        }
    }

    var isProfessionsViewHidden: Bool = false {
        didSet {
            professionsLabel.isHidden = isProfessionsViewHidden
            professionsContainerView.isHidden = isProfessionsViewHidden
        }
    }

    init(thumbnailHeight: CGFloat) {
        self.thumbnailHeight = thumbnailHeight
        super.init(frame: .zero)
        setupConstraints()
        backgroundColor = .white
    }

    override init(frame: CGRect) {
        thumbnailHeight = 0
        super.init(frame: frame)
        setupConstraints()
        backgroundColor = .white
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupConstraints() {
        subviews.forEach { $0.removeFromSuperview() }

        addSubview(arrangedScrollView)
        arrangedScrollView.pinToEdges().activate()
        arrangedScrollView.backgroundColor = .white

        let stackView = UIStackView(axis: .horizontal)
        stackView.distribution = .fillEqually
        stackView.addArranged(subviews: [ageLabel, weightLabel, heightLabel])

        arrangedScrollView.addArranged(subviews: thumbnailImageView,
                                       nameLabel,
                                       stackView,
                                       friendsLabel,
                                       friendsContainerView,
                                       professionsLabel,
                                       professionsContainerView)

        [thumbnailImageView.height(equalToConstant: thumbnailHeight),
         friendsContainerView.height(equalToConstant: 150),
         professionsContainerView.height(equalToConstant: 150)].activate()

        friendsContainerView.addSubview(friendsCollectionView)
        friendsCollectionView.pinToEdges().activate()

        professionsContainerView.addSubview(professionsCollectionView)
        professionsCollectionView.pinToEdges().activate()

        thumbnailImageView.backgroundColor = .lightGray
    }

    func set(name: String, age: String, height: String, weight: String) {
        nameLabel.text = name
        ageLabel.text = "Age:\n" + age
        heightLabel.text = "Height:\n" + height
        weightLabel.text = "Weight:\n" + weight
    }

    func setThumbnail(withUrlString urlString: String, queue: OperationQueue) {
        thumbnailImageView.setImage(withUrlString: urlString, queue: queue)
    }

    func configureFriendsCollectionView<Cell: UICollectionViewCell, Item>(dataSource: CollectionViewDataSource<Cell, Item>) {
        friendsCollectionView.register(cellType: Cell.self)
        friendsCollectionView.dataSource = dataSource
        friendsCollectionView.delegate = dataSource
    }

    func configureProfessionsCollectionView<Cell: UICollectionViewCell, Item>(dataSource: CollectionViewDataSource<Cell, Item>) {
        professionsCollectionView.register(cellType: Cell.self)
        professionsCollectionView.dataSource = dataSource
        professionsCollectionView.delegate = dataSource
    }

    func reloadFriends() {
        friendsCollectionView.reloadData()
    }

    func reloadProfessions() {
        professionsCollectionView.reloadData()
    }
}

extension UIEdgeInsets {
    init(constant: CGFloat) {
        self.init(top: constant, left: constant, bottom: constant, right: constant)
    }
}


