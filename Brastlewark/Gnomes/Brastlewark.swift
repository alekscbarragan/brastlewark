//
//  Brastlewark.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

struct Brastlewark: Codable {
    let gnomes: [Gnome]
    enum CodingKeys: String, CodingKey {
        case gnomes = "Brastlewark"
    }
}
