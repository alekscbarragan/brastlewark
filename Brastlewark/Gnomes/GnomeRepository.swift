//
//  GnomeRepository.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/10/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

final class GnomeRepository: Repository {
    typealias T = Gnome
    var items: [Gnome] = []

    var result: ((Result<[Gnome]>) -> Void)?

    private let queue: OperationQueue = {
        let q = OperationQueue()
        q.maxConcurrentOperationCount = 1
        return q
    }()

    func find(by name: String) -> Gnome? {
        let gnomeByName = items.first { (gnome) -> Bool in
            return name == gnome.name
        }

        return gnomeByName
    }

    func find(id: Int) -> Gnome? {
        let gnomeById = items.first { (gnome) -> Bool in
            return gnome.id == id
        }
        return gnomeById
    }

    func itemAt(index: Int) -> Gnome? {
        return items[index]
    }

    func update(items: [Gnome]) {
        self.items = items
    }

    func fetch(completion: ((Result<[Gnome]>) -> Void)?) {
        let operation = DataSourceOperation()
        var observer = GnomeRepositoryObserver()
        observer.result = { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let gnomes):
                self.update(items: gnomes)
                completion?(.success(gnomes))

            case .failure(let error):
                print(error)
                completion?(.failure(error))
            }
        }

        operation.add(observer: observer)
        queue.addOperation(operation)
    }
}
