//
//  GnomeCollectionViewCell.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

final class GnomeCollectionViewCell: UICollectionViewCell {

    var gridType = GridType.extended

    private let containerView = UIView()
    private let thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private let titleLabel = UILabel.templateLabel(withText: "Name", weight: .bold, textAlignment: .center)
    private let ageLabel = UILabel.templateLabel(withText: "Age", fontSize: 13, textAlignment: .center)
    private let heightLabel = UILabel.templateLabel(withText: "Height", fontSize: 13, textAlignment: .center)
    private let weightLabel = UILabel.templateLabel(withText: "Weight", fontSize: 13, textAlignment: .center)

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        contentView.backgroundColor = .white
        containerView.clipsToBounds = true
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 4.0
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowRadius = 4
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupConstraints(for gridType: GridType = .extended) {
        contentView.addSubview(containerView)

        let stackView = UIStackView(axis: .horizontal)
        stackView.distribution = .fillEqually
        stackView.addArranged(subviews: ageLabel, weightLabel, heightLabel)

        [thumbnailImageView, titleLabel, stackView].forEach { containerView.addSubview($0) }

        var constraints = containerView.pinToEdges(insets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
        constraints.append(contentsOf: [thumbnailImageView.topToSuperview(),
                                        thumbnailImageView.leftToSuperView(),
                                        thumbnailImageView.rightToSuperView(),
                                        thumbnailImageView.bottomToTop(of: titleLabel),
                                        titleLabel.leftToSuperView(),
                                        titleLabel.rightToSuperView(),
                                        titleLabel.bottomToTop(of: stackView),
                                        stackView.leftToSuperView(),
                                        stackView.rightToSuperView(),
                                        stackView.bottomToSuperview(offset: -8)]
        )

        constraints.activate()
    }

    func set(title: String, age: Int, weight: Double, height: Double) {
        titleLabel.text = title
        ageLabel.text = "Age:\n\(age)"
        weightLabel.text = String(format: "Weight:\n%.2f", weight)
        heightLabel.text = String(format: "Height:\n%.2f", height)
    }

    func setImage(withUrlString urlString: String, queue: OperationQueue) {
        thumbnailImageView.setImage(withUrlString: urlString, queue: queue)
    }
}
