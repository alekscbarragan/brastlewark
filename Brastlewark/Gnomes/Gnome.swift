//
//  Gnome.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation
import UIKit

struct Gnome: Codable {
    let age: Int
    let friends: [String]
    let height: Double
    let id: Int
    let name: String
    let professions: [String]
    let thumbnail: String
    let weight: Double
}
