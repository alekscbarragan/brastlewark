//
//  GnomesView.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

final class GnomesView: UIView {
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    convenience init(delegate: UICollectionViewDelegate & UICollectionViewDataSource) {
        self.init(frame: .zero)
        collectionView.dataSource = delegate
        collectionView.delegate = delegate
    }
    
    convenience init(collectionViewDataSource: UICollectionViewDataSource, collectionViewDelegate: UICollectionViewDelegate) {
        self.init(frame: .zero)
        collectionView.delegate = collectionViewDelegate
        collectionView.dataSource = collectionViewDataSource
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        addSubview(collectionView)
        collectionView.pinToEdges().activate()
        collectionView.register(cellType: GnomeCollectionViewCell.self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func refreshUI() {
        collectionView.reloadData()
    }
}
