//
//  GnomesViewController.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

protocol GnomesViewControllerDelegate: class {
    func gnomesViewController(_ viewController: GnomesViewController, didUpdateGnome respository: GnomeRepository)
    func gnomesViewController(_ viewController: GnomesViewController, didSelect gnome: Gnome)
}

final class GnomesViewController: BaseViewController {
    
    typealias Cell = GnomeCollectionViewCell
    typealias ConfigureCell = (Cell, Gnome, IndexPath, UICollectionView) -> Void

    // MARK: - Public Properties
    var gridType = GridType.extended
    weak var delegate: GnomesViewControllerDelegate?
    
    // MARK: - Private Properties
    private lazy var configureCell: ConfigureCell = { [weak self] (cell, gnome, indexPath, collectionView) in
        guard let self = self else { return }
        cell.set(title: gnome.name, age: gnome.age, weight: gnome.weight, height: gnome.height)
        cell.setImage(withUrlString: gnome.thumbnail, queue: self.queue)
    }
    
    private lazy var dataSource: CollectionViewDataSource<Cell, Gnome> = {
        let dataSource = CollectionViewDataSource<Cell, Gnome>(withItems: gnomeRepository.items, cellIdentifier: Cell.reuseIdentifier, configureCell: configureCell)
        return dataSource
    }()

    private var allGnomes = [Gnome]()
    private let gnomeRepository = GnomeRepository()
    private lazy var gnomesView: GnomesView = {
        let view = GnomesView(delegate: dataSource)
        return view
    }()
    
    private let queue: OperationQueue = {
        let q = OperationQueue()
        return q
    }()

    lazy var searchController = UISearchController(searchResultsController: nil)

    // MARK: - Initializers
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life View Cycle
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        view.addSubview(gnomesView)
        gnomesView.pinToEdges().activate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Brastlewark"
        configureNavigationBar()
        configureSearchController()
        dataSourceConfiguration()
        refreshDataSource()
    }

    private func configureNavigationBar() {
        let configBarButton = UIBarButtonItem(image: UIImage(named: "bar-config"), style: .plain, target: self, action: #selector(configButtonTouchUpInside))
        navigationItem.leftBarButtonItem = configBarButton

        let filterBarButton = UIBarButtonItem(image: UIImage(named: "bar-filter"), style: .plain, target: self, action: #selector(filterButtonTouchUpInside))
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "bar-search"), style: .plain, target: self, action: #selector(searchButtonTouchUpInside))

        navigationItem.rightBarButtonItems = [filterBarButton, searchBarButton]
    }

    private func configureSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Type something here to search"
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    @objc private func configButtonTouchUpInside() {

    }

    @objc private func filterButtonTouchUpInside() {
        startIndicatorViewAnimation()

        let actionSheetController = UIAlertController(title: "Sort By", message: "You can sort gnomes by their characteristics", preferredStyle: .actionSheet)

        let ageSortAction = UIAlertAction(title: "Age", style: .default) { (action) in
            let items = self.gnomeRepository.items.sorted { (lhs, rhs) -> Bool in
                return lhs.age < rhs.age
            }
            self.updateDataSource(with: items)
        }

        let heightSortAction = UIAlertAction(title: "Height", style: .default) { (action) in
            let items = self.gnomeRepository.items.sorted { (lhs, rhs) -> Bool in
                return lhs.height < rhs.height
            }
            self.updateDataSource(with: items)
        }

        let weightSortAction = UIAlertAction(title: "Weight", style: .default) { (action) in
            let items = self.gnomeRepository.items.sorted { (lhs, rhs) -> Bool in
                return lhs.weight < rhs.weight
            }
            self.updateDataSource(with: items)
        }

        let nameSortAction = UIAlertAction(title: "Name", style: .default) { (action) in
            let items = self.gnomeRepository.items.sorted { (lhs, rhs) -> Bool in
                return lhs.name < rhs.name
            }
            self.updateDataSource(with: items)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            self.stopIndicatorViewAnimation()
        }

        [ageSortAction, heightSortAction, weightSortAction, nameSortAction, cancelAction].forEach {
            actionSheetController.addAction($0)
        }

        present(actionSheetController, animated: true)
    }

    @objc private func searchButtonTouchUpInside() {
        searchController.isActive = !searchController.isActive
    }
}

// MARK: - Data Source
extension GnomesViewController {
    private func dataSourceConfiguration() {
        let gridType = GridType.column
        dataSource.sizeForItemAt { (indexPath, collectionView, viewLayout) -> CGSize in
            return gridType.size(maxWidth: collectionView.bounds.width)
        }

        dataSource.didSelectAt { [weak self] (indexPath, cell, gnome, collectionView) in
            guard let self = self else { return }
            self.delegate?.gnomesViewController(self, didSelect: gnome)
        }
    }
    
    func refreshDataSource() {
        startIndicatorViewAnimation()
        gnomeRepository.fetch { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let gnomes):
                self.updateDataSource(with: gnomes)
                self.allGnomes = gnomes
                self.delegate?.gnomesViewController(self, didUpdateGnome: self.gnomeRepository)

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func updateDataSource(with gnomes: [Gnome]) {
        dataSource.updateItems(gnomes)
        DispatchQueue.main.async {
            self.gnomesView.refreshUI()
            self.stopIndicatorViewAnimation()
        }
    }
}

extension GnomesViewController: UISearchResultsUpdating {
    var isFiltering: Bool {
        return !(searchController.searchBar.text?.isEmpty ?? true) && searchController.isActive
    }
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text?.lowercased() else { return }
        print(text)
        if searchController.isActive {
            let gnomes = allGnomes.filter { (gnome) -> Bool in
                return gnome.name.lowercased().contains(text)
            }

            updateDataSource(with: gnomes)
        }

    }
}

extension GnomesViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        updateDataSource(with: allGnomes)
    }
}
