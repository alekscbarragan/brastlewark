//
//  AppDelegate.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/6/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let coordinator: Coordinator = {
        let coordinator = Coordinator(with: UINavigationController())
        coordinator.start()
        return coordinator
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = coordinator.navigationController
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }

}
