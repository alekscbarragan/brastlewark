//
//  NetworkOperation.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

class NetworkOperation<T: Codable>: KavakOperation {
    
    var endpoint: EndpointConvertible {
        return NullEndpoint()
    }
    
    var method: HttpMethod {
        return .get
    }
    
    var params: [String: String]? {
        return nil
    }
    
    var body: [String: Any]? {
        return nil
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var generatedModel: T?
    
    override func execute() {
        // Get URL instance depending of HTTPMethod and params.
        let getURL = { () -> URL? in
            if let params = self.params, self.method == .get {
                return try? self.endpoint.toURL(params: params)
            }
            
            return try? self.endpoint.toURL()
        }
        
        guard let url = getURL() else {
            state = .finished
            return
        }
        
        let session = URLSession.shared
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 3000)
        request.httpMethod = method.rawValue
        
        if method == .post {
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            if let body = body, let json = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted) {
                request.httpBody = json
            }
        }
        
        if let headers = headers {
            for header in headers {
                request.setValue(header.1, forHTTPHeaderField: header.0)
            }
        }
        
        let task = session.dataTask(with: request) { (data, urlResponse, error) in
            guard let data = data, error == nil else {
                if let e = error {
                    self.finish(with: [e])
                } else {
                    self.finish()
                }
                return
            }
            
            self.finishedDownloadingData(data: data)
        }
        
        task.resume()
        
    }
    
    func finishedDownloadingData(data: Data) {
        let decoder = JSONDecoder()
        
        do {
            let model = try decoder.decode(T.self, from: data)
            generatedModel = model
            didGenerate(model: model)
            finish()
        } catch {
            
            finish(with: [error])
        }
    }
    
    open func didGenerate(model: T) {
        // For subclassing
    }
    
}
