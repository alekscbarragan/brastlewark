//
//  NetworkImageOperation.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import UIKit

class NetworkImageOperation: KavakOperation {
    let urlString: String
    var downloadedImage: UIImage?
    private var dataTask: URLSessionDataTask?

    init(urlString: String) {
        self.urlString = urlString
        super.init()
    }

    override func execute() {
        if let cachedImage = ImageCache.default.retrieveImage(forKey: urlString) {
            self.downloadedImage = cachedImage
            state = .finished
            return
        }

        guard let url = URL(string: urlString) else {
            state = .finished
            return
        }

        let session = URLSession.shared
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 3000)

        dataTask = session.dataTask(with: request) { (data, urlResponse, error) in

            if let error = error {
                self.finish(with: [error])
                return
            }

            guard let data = data else {
                self.finish(with: [ImageOperationError.dataNotFound])
                return
            }

            self.finishedDownloadingData(data: data)
        }

        dataTask?.resume()

    }

    func finishedDownloadingData(data: Data) {
        guard let image = UIImage(data: data) else {
            finish(with: [ImageOperationError.imageDataInvalid])
            return
        }

        ImageCache.default.store(image, forKey: urlString)
        downloadedImage = image
        finish()
    }
}
