//
//  DataSourceOperation.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

final class DataSourceOperation: NetworkOperation<Brastlewark> {
    override var endpoint: EndpointConvertible {
        return BrastlewarkEndpoint.data
    }
    
    override var method: HttpMethod {
        return .get
    }
    
    override func didGenerate(model: Brastlewark) {
        print("Did get Brastlewark with \(model.gnomes.count) gnomes.")
    }
}
