//
//  OperationObserver.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

protocol OperationObserver {
    func operationDidFinish(operation: KavakOperation)
    func operation(operation: KavakOperation, didFinishWithErrors errors: [Error])
}
