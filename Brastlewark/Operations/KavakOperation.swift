//
//  KavakOperation.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/7/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

class KavakOperation: Operation {
    enum State {
        case executing
        case finished
        case finishedWithErrors
        case awaiting
    }
    
    var errors: [Error] = []
    var observers: [OperationObserver] = []
    
    var state: State = .awaiting {
        didSet {
            willChangeValue(forKey: "isFinished")
            switch state {
            case .finished:
                for observer in observers {
                    observer.operationDidFinish(operation: self)
                }
                
            case .finishedWithErrors:
                for observer in observers {
                    observer.operation(operation: self, didFinishWithErrors: errors)
                }
                
            default:
                break
            }
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isFinished: Bool {
        return state == .finished || state == .finishedWithErrors
    }
    
    override func main() {
        state = .executing
        execute()
    }
    
    func execute() {
        fatalError("Override execute on `KavaKOperation` subclasses.")
    }
    
    func finish() {
        state = .finished
    }
    
    func finish(with errors: [Error]) {
        self.errors = errors
        state = .finishedWithErrors
    }
    
    func add(observer: OperationObserver) {
        observers.append(observer)
    }
}

public struct KavakAPI {
    static func encode(_ o: Any) -> String? {
        guard let string = o as? NSString else {
            return nil
        }
        
        return string.removingPercentEncoding
    }
}
