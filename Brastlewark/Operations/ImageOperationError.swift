//
//  ImageOperationError.swift
//  Brastlewark
//
//  Created by Alejandro Cárdenas on 4/11/19.
//  Copyright © 2019 KAVAK. All rights reserved.
//

import Foundation

enum ImageOperationError: Error {
    case dataNotFound
    case imageDataInvalid
    case unknown
}
